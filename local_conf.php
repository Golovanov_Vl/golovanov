<?php

//Режим разработчика
define ('DEVELOPMENT',true);

//Отображение ошибок SMARTY + PHP  на экране
ini_set('display_errors', 'On');
error_reporting(E_ALL);

//Выключение кэш блоков PHP
$config['tweaks']= array (
	'disable_block_cache' =>true,
);
